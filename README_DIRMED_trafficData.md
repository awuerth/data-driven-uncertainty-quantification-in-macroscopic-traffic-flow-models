# Information on DIRMED traffic data

Credits: Guillaume Costeseque (data treatment), Ibrahim Yapici (map with loop locations)

***************************************************************************************************************************************

The traffic data are stored in the excel file "TrafficData_A50.xlsx" where we can find, in order:
the loop ID (305, 304, 303, 302), the month (September (9), October (10), November (11), the day, the hour, the minute (these are 6-minute averages running from time t to t+6), 
the flow (in "number of vehicle per hour"), the average speed (in "km per hour") and the density (in "number of vehicle per km") which is a derived quantity. 
In order to ensure the relation flow = speed x density, we replaced positive flow values by 0 if the corresponding measured speed value is 0. 


The exact locations of the loops can be found in the following map: https://www.google.com/maps/d/edit?mid=1QK90p-aAOc79uYd2Nyv4LbKysc6H1vRy&usp=sharing
